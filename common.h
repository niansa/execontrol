#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define do_checked(name, ...) check_ret(#name "(" #__VA_ARGS__ ")", name(__VA_ARGS__))



static inline long check_ret(const char *fnc, long value) {
    if (value < 0) {
        perror(fnc);
        exit(-errno);
    }
    return value;
}

enum {
    EXECONTROL_CAN_EXEC
};
enum {
    EXECONTROL_UNSPECIFIED_ERROR = 0,
    EXECONTROL_OKAY = 1
};

typedef unsigned char execontrol_cmd_t;
typedef unsigned execontrol_pid_t;
typedef unsigned char execontrol_errno_t;

struct execontrol_request {
    execontrol_pid_t pid;
    execontrol_cmd_t cmd;
};
struct execontrol_response {
    execontrol_errno_t error;
};
