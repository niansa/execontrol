# execontrol

## Build and install

Building this project is extremely easy as there are no external dependencies and each executable is just a single C source file. You can easily compile both using CMake.

You can then simply copy the resulting executables to `/usr/local/bin` or wherever!

## Usage

The main `execontrol` executable is the application that will run the sandboxed environment. You just pass it a pipe file path (if the given pipe file doesn't exist, it will create one) and an application with its arguments to run. It will then wait for a "client" to connect to that file before it starts executing that command.

The `execontrol_whitelist` application is a client that accepts the path of the pipe file and the path of your whitelist (one executable per line).
